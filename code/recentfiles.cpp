// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          imageViewer
//          Copyright (C) 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/imageviewer
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QFileDialog>
#include <QMimeDatabase>
#include <QSettings>
#include <QStandardPaths>
#include <QMessageBox>

void MainWindow::resentfiles()
{
    QObject::connect(ui->menuRecentFiles, &QMenu::aboutToShow, [this]() {
        ui->actionRemoveAll->setText(tr("Remove All") + " (" + QString::number(g_recentfiles.size()) + ")");
        int first = 0;

        foreach(QAction *action, ui->menuRecentFiles->actions()) {
            if(first < 1) {
                first++;
                continue;
            }

            QMenu *delmenu = new QMenu;
            QAction *delaction = new QAction(tr("Remove"));
            QAction *runaction = new QAction(tr("Show"));
            delmenu->addAction(runaction);
            delmenu->addAction(delaction);
            action->setMenu(delmenu);
            // DELETE
            QObject::connect(delaction, &QAction::triggered, [action, runaction, delaction, this]() {
                QString straction = QDir::fromNativeSeparators(action->text());
                QUrl urlaction = QUrl(straction);
                delete delaction;
                delete runaction;
                int index = g_recentfiles.indexOf(g_openfile);
                g_recentfiles.removeAll(QDir::fromNativeSeparators(action->text()));

                if(index >= 0) {
                    if(g_recentfiles.size() == 0) {
                        label = new QLabel;
                        label->setMargin(-20);
                        label->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                        ui->scrollArea->setWidget(label);
                        QPalette palette;
                        QColor color;
                        color.setRgb(94, 92, 100);
                        palette.setColor(backgroundRole(), color);
                        ui->scrollArea->setPalette(palette);
                        label->setText(tr("No image available"));
                    } else  if(g_recentfiles.size() == index) {
                        g_openfile = g_recentfiles.at(index - 1);
                        scale(&g_openfile);
                    } else {
                        g_openfile = g_recentfiles.at(0);
                        scale(&g_openfile);
                    }
                }

                ui->actionRemoveAll->setText(tr("Remove All") + " (" + QString::number(g_recentfiles.size()) + ")");
                QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                                   EXECUTABLE_NAME);
                settings.beginGroup("Recentfiles");
                settings.setValue("recentfiles", g_recentfiles);
                settings.endGroup();
                // ipac
                action->deleteLater();
            });
            // SHOW
            QObject::connect(runaction, &QAction::triggered, [action, delaction, this]() {
                QString straction = QDir::fromNativeSeparators(action->text());
                g_openfile = straction;
                g_doscale = false;
                scale(&g_openfile);
            });
        }
    });
// ACTION REMOVE ALL
    QObject::connect(ui->actionRemoveAll, &QAction::triggered, [this]() {
        int first = 0;

        foreach(QAction *action, ui->menuRecentFiles->actions()) {
            if(first == 0) {
                first++;
                continue;
            }

            foreach(QAction *actions, ui->menuRecentFiles->actions()) {
                ui->menuRecentFiles->removeAction(action);
            }
        }

        g_recentfiles.clear();
    });
}
