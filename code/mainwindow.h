// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          imageViewer
//          Copyright (C) 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/imageviewer
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "qimagereader.h"
#include "qlabel.h"
#include <QMainWindow>
#include <QResizeEvent>

#define VERSION "0.0.1"
#define DISPLAY_NAME "imageViewer"
#define EXECUTABLE_NAME "imageviewer"

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setEndConfig();

private:
    Ui::MainWindow *ui;
    const QList<QByteArray> g_suportedimageformats = {QImageReader::supportedImageFormats()};

    void scale(QString *path);
    bool g_doscale = false;
    QString g_openfile;
    QStringList g_recentfiles;
    QLabel *label;

    //
    void file();
    void view();
    void resentfiles();
    //
    void allOpenedFiles(QStringList selectedfiles);
protected:
    void resizeEvent(QResizeEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void keyPressEvent(QKeyEvent *event) override;



public slots:
    void onWorkFinished();


};
#endif // MAINWINDOW_H
