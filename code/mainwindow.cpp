// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          imageViewer
//          Copyright (C) 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/imageviewer
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
//#include "./ui_mainwindow.h"
#include "ui_mainwindow.h"
#include <QtConcurrent/QtConcurrent>
#include <QLabel>
#include <QFutureWatcher>
#include <QPair>
#include "worker.h"
#include <QResizeEvent>
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QCoreApplication::setApplicationName(QString(DISPLAY_NAME " " VERSION));
    setWindowTitle(QCoreApplication::applicationName());
    this->setWindowIcon(QIcon(":/images/image.png"));
    ui->statusbar->setMinimumWidth(200);
    ui->statusbar->setSizeGripEnabled(true);
    file();
    view();
    resentfiles();
    // SETTINGS
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Recentfiles");
    g_recentfiles =   settings.value("recentfiles").toStringList();
    settings.endGroup();

    //
    foreach(QString ationtext, g_recentfiles) {
        ui->menuRecentFiles->addAction(QDir::toNativeSeparators(ationtext));
    }

    //
    //
}
//public slots:
void MainWindow::onWorkFinished()
{
    QFutureWatcher<QPair<QString, QPixmap>> *watcher = static_cast<QFutureWatcher<QPair<QString, QPixmap>>*>(sender());

    if(watcher) {
        QString p(watcher->result().first);
        label = new QLabel;
        label->setMargin(-20);
        label->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        ui->scrollArea->setWidget(label);
        QPalette palette;
        QColor color;
        color.setRgb(94, 92, 100);
        palette.setColor(backgroundRole(), color);
        ui->scrollArea->setPalette(palette);
        label->setPixmap(watcher->result().second);
        g_openfile = p;
    }

    // Optionally, you can delete the watcher if it's not needed anymore.
    watcher->deleteLater();
}

void MainWindow::scale(QString *path)
{
    QSize *size = new QSize(ui->scrollArea->size());
    QFuture<QPair<QString, QPixmap>> future = QtConcurrent::run([ path, size]() -> QPair<QString, QPixmap> { // Add the return type
        MyWorker *myWorker = new MyWorker;
        return myWorker->getUnscaled(path, *size);
    });
//    future.waitForFinished();
    auto *watcher = new QFutureWatcher<QPair<QString, QPixmap>>;
    watcher->setFuture(future);
    QObject::connect(watcher, &QFutureWatcher<QPair<QString, QPixmap>>::finished, this, &MainWindow::onWorkFinished, Qt::UniqueConnection);
}
// EVENT
void MainWindow::resizeEvent(QResizeEvent * event)
{
    g_doscale = true;
}
void MainWindow::mousePressEvent(QMouseEvent * event)
{
    if(g_doscale) {
        if(!g_openfile.isEmpty()) {
            g_doscale = false;
            scale(&g_openfile);
        }
    }

    event->accept();
}



void MainWindow::keyPressEvent(QKeyEvent * event)
{
    int recentfilessize = g_recentfiles.size();

    if(recentfilessize < 1) {
        return;
    }

    static int currentposition = 0;

    // SPACE
    if(event->key() == Qt::Key_Space) {
        currentposition++;

        if(currentposition > recentfilessize - 1) {
            currentposition = recentfilessize - 1;
        }

        // BACKSPACE
    } else if(event->key() == Qt::Key_Backspace) {
        currentposition--;

        if(currentposition < 0) {
            currentposition = 0;
        }
    }

    event->accept();
    g_openfile = g_recentfiles.at(currentposition);
    scale(&g_openfile);
}


void MainWindow::setEndConfig()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Recentfiles");
    settings.setValue("recentfiles", g_recentfiles);
    settings.endGroup();
}

MainWindow::~MainWindow()
{
    delete ui;
}
