// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          imageViewer
//          Copyright (C) 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/imageviewer
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef WORKER_H
#define WORKER_H



#include <QObject>
#include <QDebug>
#include <QThread>
#include <QtConcurrent>
#include <QLabel>
#include <QPair>
#include <QPixmap>
//template <typename T1, typename T2>
class MyWorker : public QObject
{
    Q_OBJECT
public:
    explicit MyWorker(QObject *parent = nullptr) : QObject(parent) {}

    QPair<QString, QPixmap> getUnscaled(QString *path, QSize size)
    {
        // CONCURRENT
        QPixmap pix(*path);
        pix = pix.scaled(size, Qt::KeepAspectRatio, Qt::SmoothTransformation);
        QPair <QString, QPixmap> par;
        par.first = *path;
        par.second = pix;
        return par;
    }



};
#endif // WORKER_H
