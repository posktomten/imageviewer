<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="54"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="60"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="66"/>
        <source>Recent files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="77"/>
        <source>Open...</source>
        <translation>Öppna...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="82"/>
        <source>Origonal size</source>
        <translation>Ursprunglig storlek</translation>
    </message>
    <message>
        <location filename="../file.cpp" line="41"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="42"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="43"/>
        <source>Open the image file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="47"/>
        <source>All suported files </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="55"/>
        <source>All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="58"/>
        <source>All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="76"/>
        <source>:&lt;br&gt; The file does not exist or cannot be opened.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="84"/>
        <source>:&lt;br&gt; The file extension is not supported.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="92"/>
        <source>:&lt;br&gt; Incorrect mime type (no image file).&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file.cpp" line="106"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="87"/>
        <location filename="../recentfiles.cpp" line="30"/>
        <location filename="../recentfiles.cpp" line="52"/>
        <source>Remove All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../recentfiles.cpp" line="40"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../recentfiles.cpp" line="41"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
