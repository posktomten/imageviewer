// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          imageViewer
//          Copyright (C) 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/imageviewer
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QFileDialog>
#include <QMimeDatabase>
#include <QSettings>
#include <QStandardPaths>
#include <QMessageBox>

void MainWindow::file()
{
    // OPEN FILES (QFileDialog)
    QObject::connect(ui->actionOpen, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        QString filter(tr("All suported files ") + "(");

        foreach(QByteArray b, g_suportedimageformats) {
            filter.append("*." + b + " ");
        }

        settings.beginGroup("Path");
        QString openpath = settings.value("openpath", QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)).toString();
        QString selectednamefilter = settings.value("selectednamefilter", filter).toString();
        settings.endGroup();
        QFileDialog filedialog(this);
        //        filedialog.setWindowFlags(filedialog.windowFlags() | Qt::WindowStaysOnTopHint);
        int x = this->x();
        int y = this->y();
        filedialog.setDirectory(openpath);
        filedialog.setLabelText(QFileDialog::Accept, tr("Open"));
        filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
        filedialog.setWindowTitle(tr("Open the image file"));
        filedialog.setGeometry(x + 50, y + 50, 900, 550);
        filedialog.setFileMode(QFileDialog::ExistingFiles);
        QStringList filters;
        QStringList exclusiveFilter;

        foreach(QByteArray b, g_suportedimageformats) {
            exclusiveFilter.append("(*." + b + ") ");
        }

        filter.append(")");
#ifdef Q_OS_LINUX
        filters << selectednamefilter << filter << exclusiveFilter << QString(tr("All files (*.*)"));
#endif
#ifdef Q_OS_WIN
        filters << selectednamefilter << filter << exclusiveFilter << QString(tr("All files (*)"));
#endif
        filedialog.setNameFilters(filters);

        if(!filedialog.exec()) {
            return;
        }

        QDir dir(filedialog.directory());
        settings.beginGroup("Path");
        settings.setValue("openpath", dir.absolutePath());
        settings.setValue("selectednamefilter", filedialog.selectedNameFilter());
        settings.endGroup();
        ///
        QStringList selectedfiles = filedialog.selectedFiles();
        allOpenedFiles(selectedfiles);
    });
}

void MainWindow::allOpenedFiles(QStringList selectedfiles)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    QString errormessage;
    QString errormessages;
    bool erroropened = false;

    foreach(QString selectedfile, selectedfiles) {
        QFileInfo fi(selectedfile);
        QString filename = fi.fileName();

        if(!(fi.isFile() && fi.exists())) {
            errormessage.append(filename + tr("<br> The file does not exist or cannot be opened.<br>"));
            erroropened = true;
        }

        QString extension = selectedfile.mid(selectedfile.lastIndexOf(".") + 1);
        extension = extension.toLower();

        if(!g_suportedimageformats.contains(extension)) {
            errormessage.append(filename + tr("<br> The file extension is not supported.<br>"));
            erroropened = true;
        }

        QMimeDatabase db;
        QMimeType mime = db.mimeTypeForFile(selectedfile);

        if(!mime.isValid()) {
            errormessage.append(filename + tr("<br> Incorrect mime type (no image file).<br>"));
            erroropened = true;
        }

        if(erroropened) {
            errormessages.append(errormessage + "<br>");
            errormessage.clear();
            erroropened = false;
            selectedfiles.removeAll(selectedfile);
        } else {
            if(!g_recentfiles.contains(selectedfile)) {
                g_recentfiles << selectedfile;
                //
                ui->menuRecentFiles->addAction(QDir::toNativeSeparators(selectedfile));
                //
            }
        }
    }

    if(!errormessages.isEmpty()) {
        QString strformat;
        int counter = g_suportedimageformats.size();

        foreach(QByteArray formats, g_suportedimageformats) {
            if(counter < 2) {
                strformat.append(tr("and ") + "." + formats);
            } else {
                strformat.append("." + formats + ", ");
                counter--;
            }
        }

        QMessageBox msgBox;
        //        msgBox.setWindowFlags(msgBox.windowFlags() | Qt::WindowStaysOnTopHint);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(errormessages + tr("Suported exensions is:") + "<br>" + strformat);
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
    }

    int size = selectedfiles.size();

    if(size > 0) {
        QString *tmp = new QString(selectedfiles.at(size - 1));
        g_doscale = false;
        scale(tmp);
        settings.beginGroup("Recentfiles");
        settings.setValue("recentfiles", g_recentfiles);
        settings.endGroup();
    }
}
