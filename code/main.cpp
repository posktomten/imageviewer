// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          imageViewer
//          Copyright (C) 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/imageviewer
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "mainwindow.h"
#include <QApplication>
#include <QSettings>
#include <QStyleFactory>
#include <QTranslator>

int main(int argc, char *argv[])
{
#ifdef Q_OS_LINUX
    QCoreApplication::setAttribute(Qt::AA_DontUseNativeDialogs, true);
#endif
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Language");
    const QString currentLocale(QLocale::system().name());
    QString language = settings.value("language", currentLocale).toString();
    settings.endGroup();
    QApplication *app = new QApplication(argc, argv);
    QApplication::setStyle(QStyleFactory::create("Fusion"));
    QTranslator translator;

    if(language == "custom") {
        settings.beginGroup("Language");
        QString customlanguagepath = settings.value("customlanguagepath").toString();
        settings.endGroup();

        if(translator.load(customlanguagepath)) {
            QApplication::installTranslator(&translator);
        }
    } else if(translator.load(":/i18n/imageviewer_" + language + ".qm")) {
        app->installTranslator(&translator);
    }

    MainWindow *mMainWindow  = new MainWindow;
    mMainWindow->show();
    // End config
    QObject::connect(app, &QCoreApplication::aboutToQuit,
                     [mMainWindow]() -> void { mMainWindow->setEndConfig(); });
    return app->exec();
}

